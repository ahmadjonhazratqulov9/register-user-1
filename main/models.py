from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    age = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=255)
    job = models.CharField(max_length=244)